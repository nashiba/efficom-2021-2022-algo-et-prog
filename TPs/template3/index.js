///// Définition de fonctions utilitaires qui
///// pourront être utilisées dans notre programme principal
const rl = require('readline');

/**
 * Read pose une question à l'utilisateur qui doit renseigner sa réponse
 *
 * @param {string} question une chaîne de caractère qui sera affichée
 *                      à l'utilisateur pour lui indiquer quoi écrire.
 * @returns {Promise<string>} Une promesse de réponse de l'utilisateur en chaîne de caractère
 *
 * @example `const reponse = await read('Donnez votre âge')`
 */
function read(question) {
  return new Promise((resolve) => {
    const r = rl.createInterface({
      input: process.stdin,
      output: process.stdout});
    r.question(question + '\n> ', function(answer) {
      r.close();
      resolve(answer);
    });
  })
}

/**
 * Print écrit sur la sortie standard du texte à destination de
 * l'utilisateur du programme.
 *
 * @param {string} sentence la chaîne de caractère qui sera affichée.
 *
 * @example `print('Le résultat de 2 + 2 est ' + (2 + 2))`
 */
function print(sentence) {
  console.log(sentence)
}

/**
 * Retourne un nombre entier aléatoire entre 0 et 100
 *
 * @example `const value = random() // value contiendra une valeur aléatoire`
 */
 function random () {
  const min = Math.ceil(0);
  const max = Math.floor(100);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

///// Fin de définition de nos utilitaires

///// Début du code de notre programme principal

async function main () {
  // ... corps de notre programme
  print('Hello world !')
}

///// Fin du code de notre programme

// Execution du programme
main()


