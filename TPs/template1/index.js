///// Définition des imports de modules et fonctions utilitaires qui
///// pourront être utilisées dans notre programme principal
const { read, print } = require('./in_out')

///// Fin de définition et import de nos utilitaires

///// Début du code de notre programme principal
async function main () {
  // ... corps de notre programme
  print('Hello world !')
}

///// Fin du code de notre programme

// Execution du programme
main()
