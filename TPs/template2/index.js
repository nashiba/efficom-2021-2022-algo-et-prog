
// 1. Vous disposez de la fonction `prompt` qui ouvre une modale dans le navigateur
// pour demander à l'utilisateur de saisir une valeur. Cette fonction prend en paramètre
// une chaîne de caractère qui sera affichée à l'utilisateur et retourne
// la valeur saisie en tant que chaîne de caractère.

// 2. Vous disposez de la fonction `alert` qui ouvre une modale dans le navigateur
// pour simplement afficher une information à l'utilisateur. Cette fonction prend en paramètre
// une chaîne de caractère qui sera affichée à l'utilisateur.

// 3. Vous disposez de la fonction `console.log` qui affiche une information à l'utilisateur
// dans la console du navigateur. Cette fonction prend en paramètre
// une valeur qui sera affichée à l'utilisateur dans la console.

/////

async function main () {
  // ... contenu de la fonction
  console.log('Hello world !')
}

/////

// Execution
main()
