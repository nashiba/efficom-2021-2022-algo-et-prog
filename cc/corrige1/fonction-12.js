function auCarre(tab) {
  const tableauResultat = []

  for (let index = 0; index < tab.length; index++) {
    const elementAuCarre = tab[index] * tab[index]
    tableauResultat.push(elementAuCarre)
  }

  return tableauResultat
}

console.log(auCarre([1, 2, 3, 4]))
