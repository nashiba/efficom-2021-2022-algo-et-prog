function factureEssence(L) {
  let prix = 0
  let palier1 = 8
  let palier2 = 8 + 12

  for (let l = 0; l < L; l++) {
    if (l < palier1) {
      prix = prix + 1.655
    } else if (l < palier2) {
      prix = prix + 1.5
    } else {
      prix = prix + 1.3
    }
  }

  return prix
}

console.log('pour 5L le prix est:', factureEssence( 5 ))
console.log('pour 15L le prix est:', factureEssence( 15 ))
console.log('pour 27L le prix est:', factureEssence( 27 ))
