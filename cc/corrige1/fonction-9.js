function factureEssence(L) {
  let nb8l = 0
  let nb12l = 0
  let nbReste = 0

  if (L <= 8) {
    nb8l = L

  } else {
    nb8l = 8

    if (L > 20) {
      nb12l = 12
      nbReste = L - 20

    } else {
      nb12l = L - 8
    }
  }

  let prix = (nb8l * 1.655) + (nb12l * 1.5) + (nbReste * 1.3)
  return prix
}

console.log('pour 5L le prix est:', factureEssence( 5 ))
console.log('pour 15L le prix est:', factureEssence( 15 ))
console.log('pour 27L le prix est:', factureEssence( 27 ))
