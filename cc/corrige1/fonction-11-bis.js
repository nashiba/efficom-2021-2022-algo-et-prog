function construitTableau(tab, indices) {
  const tableauResultat = []

  for (let index = 0; index < indices.length; index++) {
    let indiceAPrendre = indices[index]
    if (indiceAPrendre >= 0 && indiceAPrendre < tab.length) {
      tableauResultat.push(tab[indiceAPrendre])
    }
  }

  return tableauResultat
}

console.log(construitTableau( ["lun", "mar", "mer", "jeu", "ven"], [1, 3, 4, 12] ))
